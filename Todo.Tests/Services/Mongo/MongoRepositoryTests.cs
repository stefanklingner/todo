﻿using System;
using System.Linq;
using FluentAssertions;
using FluentAssertions.Common;
using Todo.Models;
using Todo.Services;
using Todo.Services.Mongo;
using Xunit;

namespace Todo.Tests.Services.Mongo
{
    [Collection("MongoTestCollection")]
    public class MongoRepositoryTests
    {
        #region Felder

        private readonly IGenericRepository<TaskList> _mongoRepository;

        #endregion

        public MongoRepositoryTests(MongoFixture mongoFixture)
        {
            _mongoRepository = new MongoRepository<TaskList>(mongoFixture.MongoAccessTasks);
        }

        [Fact]
        public void ST_AddATask()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1235"
            };

            taskList.Tasks.Add(task);

            _mongoRepository.Add(taskList);
        }

        [Fact]
        public void ST_FindATaskList()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1235"
            };

            taskList.Tasks.Add(task);

            var addedTaskList = _mongoRepository.Add(taskList);
            addedTaskList.Should().NotBeNull();

            var foundTask = _mongoRepository.FindBy(addedTaskList.Id);
            foundTask.Id.IsSameOrEqualTo(addedTaskList.Id);
        }

        [Fact]
        public void ST_UpdateATask()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1235"
            };

            taskList.Tasks.Add(task);

            var taskToUpdateList = _mongoRepository.Add(taskList);
            taskToUpdateList.Should().NotBeNull();

            var task2 = new Task
            {
                Description = "test task 2",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var newDescription = "Updated Task";
            taskToUpdateList.Tasks[0].Description = newDescription;
            taskToUpdateList.Tasks.Add(task2);

            _mongoRepository.Update(taskToUpdateList);

            var foundTaskList = _mongoRepository.FindBy(taskToUpdateList.Id);

            foundTaskList.Tasks.Count.ShouldBeEquivalentTo(2);
            foundTaskList.Tasks[0].Description.Should().Be(newDescription);
            foundTaskList.Tasks[1].Description.Should().Be(task2.Description);

        }

        [Fact]
        public void ST_GetAllTaskLists()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1235"
            };

            taskList.Tasks.Add(task);

            var addedTaskList = _mongoRepository.Add(taskList);
            addedTaskList.Should().NotBeNull();

            var taskLists = _mongoRepository.GetAll().ToList();
            taskLists.Count.Should().BeGreaterThan(0);
        }

        [Fact]
        public void ST_RemoveATaskList()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1235"
            };

            taskList.Tasks.Add(task);

            var taskListToRemove = _mongoRepository.Add(taskList);
            taskListToRemove.Should().NotBeNull();

            _mongoRepository.Remove(taskListToRemove);

            var foundTask = _mongoRepository.FindBy(taskListToRemove.Id);
            foundTask.Should().BeNull();
        }
    }
}