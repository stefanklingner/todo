﻿using System;
using System.Linq;
using Todo.Models;
using Todo.Services.Mongo;
using Xunit;
using FluentAssertions;
using FluentAssertions.Common;

namespace Todo.Tests.Services.Mongo
{
    [Collection("MongoTestCollection")]
    public class MongoAccessTests
    {
        private readonly MongoAccess<TaskList> _mongoAccess;

        public MongoAccessTests(MongoFixture mongoFixture)
        {
            _mongoAccess = mongoFixture.MongoAccessTasks;
        }

        [Fact]
        public void ST_CreateATaskList()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1234"
            };

            taskList.Tasks.Add(task);

            var newTaskList = _mongoAccess.Create(taskList).Result;

            Assert.False(string.IsNullOrEmpty(newTaskList.Id));
            Assert.True(newTaskList.Tasks[0].Description == task.Description);
        }

        [Fact]
        public void ST_ReadATaskById()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1234"
            };

            taskList.Tasks.Add(task);

            var newTask = _mongoAccess.Create(taskList).Result;

            Assert.False(string.IsNullOrEmpty(newTask.Id));
            Assert.True(newTask.Tasks[0].Description == task.Description);

            var result = _mongoAccess.GetById(newTask.Id).Result;

            Assert.NotNull(result);
            Assert.Equal(result.Id, newTask.Id);
        }

        [Fact]
        public void ST_UpdateATaskList()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };
            var taskList = new TaskList
            {
                UserId = "1234"
            };

            taskList.Tasks.Add(task);

            var createdTaskList = _mongoAccess.Create(taskList).Result;

            createdTaskList.Should().NotBeNull();

            const string newDescription = "Updated Task";
            createdTaskList.Tasks[0].Description = newDescription;

            var task2 = new Task
            {
                Description = "test task 2",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            createdTaskList.Tasks.Add(task2);

            _mongoAccess.Update(createdTaskList.Id, createdTaskList);

            var updatedTaskList = _mongoAccess.GetById(createdTaskList.Id);

            updatedTaskList.Result.Tasks.Count.Should().IsSameOrEqualTo(2);
            updatedTaskList.Result.Tasks[0].Description.Should().IsSameOrEqualTo(newDescription);
            updatedTaskList.Result.Tasks[1].Description.Should().IsSameOrEqualTo(task2.Description);

        }

        [Fact]
        public void ST_GetAllTasks()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1234"
            };
            taskList.Tasks.Add(task);

            var createdTaskList = _mongoAccess.Create(taskList).Result;

            createdTaskList.Should().NotBeNull();

            var allTasks = _mongoAccess.GetAll().Result.ToList();
            allTasks.Count.Should().BeGreaterThan(0);
        }

        [Fact]
        public void ST_RemoveATaskList()
        {
            var task = new Task
            {
                Description = "test task",
                When = DateTime.Now.AddDays(1),
                Deleted = false,
                Done = false
            };

            var taskList = new TaskList
            {
                UserId = "1234"
            };

            var createdTask = _mongoAccess.Create(taskList).Result;

            createdTask.Should().NotBeNull();

            _mongoAccess.Remove(createdTask.Id);

            var foundTask = _mongoAccess.GetById(createdTask.Id).Result;
            foundTask.Should().BeNull();
        }

    }
}
