﻿using System;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using Todo.Models;
using Todo.Services.Mongo;
using Xunit;

namespace Todo.Tests.Services.Mongo
{
    //https://xunit.github.io/docs/shared-context.html#collection-fixture
    public class MongoFixture : IDisposable
    {
        public MongoFixture()
        {
            BsonClassMap.RegisterClassMap<TaskList>(cm =>
            {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            var connectionString = new Todo.Services.ConnectionString("mongodb://localhost:27017");
            var client = new MongoClient(connectionString.ConnString);

            MongoAccessTasks = new MongoAccess<TaskList>(client, new MongoCollection("tasks"));

        }

        public MongoAccess<TaskList> MongoAccessTasks { get; private set; }

        public void Dispose()
        {
            // ... clean up ...
        }
    }

    [CollectionDefinition("MongoTestCollection")]
    public class MongoTestCollection : ICollectionFixture<MongoFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
