﻿using System.Collections.Generic;
using Todo.Services;

namespace Todo.Models
{
    public class TaskList : IItemWithId
    {
        public TaskList()
        {
            Tasks = new List<Task>();
        }
        public string Id { get; set; }
        public string UserId { get; set; }
        public List<Task> Tasks { get; set; }

    }
}
