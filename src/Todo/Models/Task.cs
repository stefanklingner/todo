using System;
using Todo.Services;

namespace Todo.Models
{
    public class Task : IItemWithId
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public DateTime When { get; set; }
        public bool Done { get; set; }
        public bool Deleted { get; set; }
    }
}
