﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using Todo.Models;
using Todo.Services;
using Todo.Services.Mongo;

namespace Todo.Extensions
{
    //http://asp.net-hacker.rocks/2016/02/17/dependency-injection-in-aspnetcore.html
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
            BsonClassMap.RegisterClassMap<TaskList>(cm =>
            {
                cm.AutoMap();
                cm.MapIdMember(c => c.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
            });

            var connectionString = new ConnectionString("mongodb://localhost:27017");
            var client = new MongoClient(connectionString.ConnString);

            var mongoAccess = new MongoAccess<TaskList>(client, new MongoCollection("tasks"));
    
            services.AddScoped(typeof(IGenericRepository<TaskList>), 
                p => new MongoRepository<TaskList>(mongoAccess));

            return services;
        }
    }
}
