
namespace Todo.Services
{
    public interface IItemWithId
    {
        string Id { get; set; }
    }
}
