using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Todo.Services
{
    /// <summary>
    ///     This Repo fakes the Database Access by using a dictionary 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RepositoryWithDictionary<T> : IGenericRepository<T> where T : IItemWithId
    {
        private static readonly ConcurrentDictionary<string, T> Elements =
              new ConcurrentDictionary<string, T>();

        public T Add(T item)
        {
            item.Id = Guid.NewGuid().ToString();   
            Elements[item.Id] = item;
            return item;
        }

        public T FindBy(string id)
        {
            T item;
            Elements.TryGetValue(id, out item);
            return item;
        }

        public IEnumerable<T> GetAll()
        {
           return Elements.Values;
        }

        public void Remove(T item)
        {
            T removedItem;
            Elements.TryRemove(item.Id, out removedItem);
        }

        public void Update(T item)
        {
            Elements[item.Id] = item;
        }
    }
}