﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.Services.Mongo
{
    public class MongoCollection
    {
        public MongoCollection(string collectionName)
        {
            if (string.IsNullOrEmpty(collectionName))
            {
                throw new ArgumentNullException(nameof(collectionName));
            }
            CollectionName = collectionName;
        }

        public string CollectionName { get; }
    }
}
