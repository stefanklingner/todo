﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Todo.Services.Mongo
{
    public class MongoAccess<T> where T : IItemWithId
    {
        private readonly IMongoDatabase _db;
        private readonly string _collectionName;
       
        public MongoAccess(IMongoClient client, MongoCollection collectionName)
        {
            _db = client.GetDatabase("test");
            _collectionName = collectionName.CollectionName;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            var collection = _db.GetCollection<T>(_collectionName);
            var filter = new BsonDocument();

            var list = new List<T>();

            using (var cursor = await collection.FindAsync(filter))
            {
                while (await cursor.MoveNextAsync())
                {
                    var batch = cursor.Current;
                    list.AddRange(batch);
                }
            }
             
            return list;
        }

        public async Task<T> GetById(string id)
        {
            var collection = _db.GetCollection<T>(_collectionName);
            var filter = Builders<T>.Filter.Eq("_id", id);
            var result = await collection.Find(filter).ToListAsync();
            return result.FirstOrDefault();
        }

        public async Task<T> Create(T p)
        {
            await _db.GetCollection<T>(_collectionName).InsertOneAsync(p);
            return p;
        }

        public async void Update(string id, T p)
        {
            var collection = _db.GetCollection<T>(_collectionName);
            p.Id = id;
            var filter = Builders<T>.Filter.Eq("_id", id);
            await collection.ReplaceOneAsync(filter,p);
        }

        public async void Remove(string id)
        {
            var collection = _db.GetCollection<T>(_collectionName);
            var filter = Builders<T>.Filter.Eq("_id", id);
            await collection.DeleteOneAsync(filter);
        }

    }
}
