﻿using System;
using System.Collections.Generic;

namespace Todo.Services.Mongo
{
    public class MongoRepository<T> : IGenericRepository<T> where T : IItemWithId
    {
        private readonly MongoAccess<T> _mongoAccess;

        public MongoRepository(MongoAccess<T> mongoAccess)
        {
            if (mongoAccess == null)
            {
                throw new ArgumentNullException(nameof(mongoAccess));
            }
            _mongoAccess = mongoAccess;
        } 

        public T Add(T item)
        {
            //todo: refactor
            return _mongoAccess.Create(item).Result;
        }

        public IEnumerable<T> GetAll()
        {
            return _mongoAccess.GetAll().Result;
        }

        public T FindBy(string id)
        {
            return _mongoAccess.GetById(id).Result;
        }

        public void Remove(T item)
        {
           _mongoAccess.Remove(item.Id);
        }

        public void Update(T item)
        {
            _mongoAccess.Update(item.Id, item);
        }
    }
}