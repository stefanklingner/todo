
using MongoDB.Bson;

namespace Todo.Services.Mongo
{
    public interface IMongoItem
    {
        ObjectId Id { get; set; }
    }
}
