
using System.Collections.Generic;

namespace Todo.Services
{
    public interface IGenericRepository<T>
    {
        T Add(T item);
        IEnumerable<T> GetAll();
        T FindBy(string id);
        void Remove(T id);
        void Update(T item);
    }
}