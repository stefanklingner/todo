
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using Todo.Models;
using Todo.Services;

namespace Todo.Controllers
{
    [Route("api/[controller]")]
    public class TodoController : Controller
    {
        public IGenericRepository<TaskList> TodoItems { get; set; }

        public TodoController(IGenericRepository<TaskList> todoItems)
        {
            TodoItems = todoItems;
        }

        [HttpGet]
        public IEnumerable<TaskList> GetAll()
        {
            return TodoItems.GetAll();
        }

        [HttpGet("{id}", Name = "GetTodo")]
        public IActionResult GetById(string id)
        {
            var item = TodoItems.FindBy(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskList item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            TodoItems.Add(item);
            return CreatedAtRoute("GetTodo", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] TaskList item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            var todo = TodoItems.FindBy(id);
            if (todo == null)
            {
                return NotFound();
            }

            TodoItems.Update(item);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var todo = TodoItems.FindBy(id);
            if (todo == null)
            {
                return NotFound();
            }

            TodoItems.Remove(todo);
            return new NoContentResult();
        }
    }
}   